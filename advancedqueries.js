// [SECTION] Comparison Query Operators

	// $gt/$gte operators

	/*
		- Allow us to find documents that have field number greater than or equal to a specified value

		Syntax:
		db.collectionName.find({ field: {$gt/$gte : value} });

	*/
	db.users.find({ age: {$gt: 50} });
	db.users.find({ age: {$gte: 50} });

	// $lt/lte operators
	/*
		- Allow us to find documents that have field number less than or equal to a specified value
		Syntax:
		db.collectionName.find({ field: {$lt/$lte : value} });
	*/
	db.users.find({ age: {$lt: 50} });
	db.users.find({ age: {$lte: 50} });

	// $ne operator
	/*
		- Allows us to find documents that have field number not equal to a specified value
		Syntax:
		db.collectionName.find({ field: {$ne : value} });
	*/

	db.users.find({ age: {$ne: 82} });

	// $in operator
	/*
		Allows us to find documents with specific match criteria in one field using different values
		Syntax:
		db.collectionName.find({ field: {$in : value} });
	*/
	db.users.find({ lastName: {$in: ["Hawking", "Doe"]}});
	db.users.find({ courses: {$in: ["HTML", "React"]}});


// [SECTION] Logical Query Operators

	// $or operator
	/*
		- Allows us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
		db.collectionName.find({ $or: [ {fieldA: valueA}, {fieldB: valueB}] });
	*/

	// multiple field value pairs
	db.users.find({$or: [ {firstName: "Neil"}, {age: 25} ] });
	db.users.find({$or: [ {firstName: "Neil"}, {age: {$gt: 30} } ] });

	// $and operator
	/*
		- Allows us to find documents matching multiple criteria in a single field
		Syntax:
		db.collectionName.find({ $and: [ {fieldA: valueA}, {fieldB: valueB}] });
	*/
	db.users.find({ $and: [ {age: {$ne:82}}, {age: {$ne: 76}} ] });

// [SECTION] Field Projection

	// Inclusion
	/*
		- Allows us to include/add specific fields only when retrieving documents
		- The value provided is 1 to denote that the field/s is/are being included
		Syntax:
		db.collectionName.find({criteria}, {field: 1})
	*/
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	);

	// Exclusion
	/*
		- Allows us to exclude/removespecific fields only when retrieving documents
		- The value provided is 0 to denote that the field/s is/are being excluded
		Syntax:
		db.collectionName.find({criteria}, {field: 0})
	*/
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			contact: 0,
			department: 0
		}
	);

	// Suppressing the ID field
	/*
		- Allows us to exclude the "_id" field when retrieving documents.
		- When using field projection, the field inclusion and exclusion may not be used at the same time
		- Excluding the "_id" field is the only exception to the rule
	*/
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1,
			_id: 0
		}
	);

	// Returning specific field in embedded documents
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			"contact.phone": 1
		}
	);

	// Suppressing Specific fields in embedded documents
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			"contact.phone": 0
		}
	);

	// Project Specific Array elements in the returned array
	// $slice operator allows us to retrieve only 1 element that matches the search criteria

	db.users.find(
		{
			namearr: {
				namea: "juan"
			}
		},
		{
			namearr: {$slice: 1}
		}
	);
	// Example 2
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			courses: {$slice: 1}
		}
	);

	// $regex operator - allows us to find documents that match a specific string patter using regular expressions
	/*
		Syntax:
		db.collectionName.find({field: {$regex: "pattern", $options: "$optionValue"}})
	*/

	// Case sensitive query
	db.users.find({firstName: {$regex: "N"}});

	// Case insensitive query
	db.users.find({ firstName: {$regex: "j", $options: "$i"}});
